import { ArgumentsHost, Catch, HttpServer, NotFoundException } from "@nestjs/common";
import { BaseExceptionFilter } from "@nestjs/core";
import { resolve, join } from "path";

@Catch(NotFoundException)
export class AllExceptionsFilter extends BaseExceptionFilter {
    constructor(applicationRef: HttpServer) {
        super(applicationRef);
    }

    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const req = ctx.getRequest();

        if (req.path && req.path.startsWith("/api")) {
            // API 404, serve default nest 404:
            super.catch(exception, host);
        } else {
            // client access, let the SPA handle:
            response.sendFile(resolve(join(__dirname, "..", "client", "build", "index.html")));
        }
    }
}

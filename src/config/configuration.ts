export default () => ({
    port: parseInt(process.env.PORT, 10) || 5000,
    isProduction: process.env.NODE_ENV === "production",
});

import { Injectable } from "@nestjs/common";

@Injectable()
export class CoreService {
    post(post: string): string {
        return `I received your POST request. This is what you sent me: ${post}`;
    }

    get(): Resp {
        return { express: "Hello From Express" };
    }
}

export interface Resp {
    express: string;
}

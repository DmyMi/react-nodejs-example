import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { CoreService, Resp } from "./core.service";

@Controller("api")
export class CoreController {
    constructor(private readonly coreService: CoreService) {}

    @Post("world")
    async post(@Body() postDto: PostStuff): Promise<string> {
        return this.coreService.post(postDto.post);
    }

    @Get("hello")
    async hello(): Promise<Resp> {
        return this.coreService.get();
    }
}

interface PostStuff {
    post: string;
}

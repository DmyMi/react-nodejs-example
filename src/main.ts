import { ValidationPipe } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { HttpAdapterHost, NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { AllExceptionsFilter } from "./redirect-client.filter";
import { join } from "path";
import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface";

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    app.useGlobalPipes(new ValidationPipe());
    const configService = app.get(ConfigService);
    if (configService.get("isProduction")) {
        app.useStaticAssets(join(__dirname, "..", "client", "build"));
        const refHost = app.get(HttpAdapterHost);
        const httpServer = refHost.httpAdapter.getHttpServer();
        app.useGlobalFilters(new AllExceptionsFilter(httpServer));
    }
    await app.listen(configService.get("port"));
    console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();

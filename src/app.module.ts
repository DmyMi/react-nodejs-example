import { Module } from "@nestjs/common";
import { CoreModule } from "./core/core.module";
import { ConfigModule } from "@nestjs/config";
import configuration from "./config/configuration";

@Module({
    imports: [
        CoreModule,
        ConfigModule.forRoot({
            load: [configuration],
            ignoreEnvFile: true,
            isGlobal: true,
        }),
    ],
})
export class AppModule {}
